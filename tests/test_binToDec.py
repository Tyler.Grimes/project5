from binaryConverter import binToDecList

def test_1():
    assert binToDecList([0, 1, 1]) == 3

def test_2():
    assert binToDecList([0]) == 0